import dotenv from 'dotenv';
import app from './index';
import config from './config';

dotenv.config({ path: 'variables.env' });

app.listen(config.port, () => {
  console.log(`Listening on port ${config.port}`);
});
