import Koa from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import morgan from 'koa-morgan';
import cors from '@koa/cors';
import graphRouter from './routes/graph';
import passportInit from './services/twitterPassport';
import twitterAuthRouter from './routes/twitterAuth';
import twitterArouter from './routes/twitter';

// TODO configure the app to work with dotenv properly

passportInit();
const app = new Koa();
const mainRouter = new Router();

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.body = { message: err.message };
    ctx.status = err.status || 500;
  }
});
const corsOption = {
  // origin: '*',
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};
app.use(cors(corsOption));

app.use(bodyParser());
app.use(morgan('combined'));

mainRouter.use('/api/graph', graphRouter.routes(), graphRouter.allowedMethods());
mainRouter.use('/auth', twitterAuthRouter.routes(), twitterAuthRouter.allowedMethods());
mainRouter.use('/twitter', twitterArouter.routes(), twitterArouter.allowedMethods());
app.use(mainRouter.allowedMethods());
app.use(mainRouter.routes());

app.on('error', err => {
  console.log('server error', err);
});

export default app;
