import passport from 'koa-passport';
import TwitterTokenStrategy from 'passport-twitter-token';
import User from '../db/models/user';

const twitterAuth = () => {
  passport.use(
    new TwitterTokenStrategy(
      {
        consumerKey: process.env.TWI_CONSUMER_KEY,
        consumerSecret: process.env.TWI_CONSUMER_SECRET,
        includeEmail: true
      },
      (token, tokenSecret, profile, done) => {
        User.upsertTwitterUser(token, tokenSecret, profile, (err, user) => done(err, user));
      }
    )
  );
};

export default twitterAuth;
