import { EventEmitter } from 'events';
import colors from 'colors';
import twitterGet from '../helpers/twitter-get';
import topicValidator from '../helpers/topic-validator';
import accountRanker from '../helpers/accountRanker';
import {
  findOneAccount,
  insertManyAccounts,
  createAccount,
  updateAccount
} from '../db/repositories/mongo/accounts';

const validator = topicValidator({ text: false });

const MIN_FOLLOWERS_COUNT = 2000;
// let parsedFriendsCounter = 0;

class FriendsListParser extends EventEmitter {
  constructor(User) {
    super();
    this.CurrUser = User;
    this.isParserRunning = true;
    this.parsedFriendsCounter = 0;
    this.on('parser-stop', () => this.stop());
  }

  async parse(_params) {
    const params = {
      screen_name: this.CurrUser.screen_name,
      skip_status: true,
      include_user_entities: false,
      ..._params
    };
    const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
      '/friends/list.json',
      params
    );
    const { users, next_cursor: nextCursor } = data;
    if (users.length === 0) {
      console.log('Returned users array is empty, finishing up...'.green);
      await this.updateAndFinish();
      return;
    }
    this.parsedFriendsCounter += users.length;
    const filteredUsers = users
      .filter(user => user.followers_count >= MIN_FOLLOWERS_COUNT && validator(user.description))
      .map(
        ({
          screen_name,
          location,
          description,
          followers_count,
          friends_count,
          verified,
          statuses_count,
          id_str: user_twitter_id
        }) => ({
          user_twitter_id,
          screen_name,
          location,
          description,
          followers_count,
          friends_count,
          verified,
          statuses_count,
          ranking: accountRanker({ followers_count, friends_count, verified, statuses_count })
        })
      );

    try {
      if (filteredUsers.length !== 0) {
        await insertManyAccounts(filteredUsers, { ordered: false });
        console.log(`*** ==> Inserting ${filteredUsers.length} user accounts <== ***`.red);
      }
    } catch (e) {
      console.log('===========> ERROR'.bgRed);
      console.log(e.message);
      // console.log(filteredUsers);
      // return;
    }
    console.log('Remaining number => ', `${remainingReqNum}`.yellow);
    console.log('Friends parsed total => ', ` ${this.parsedFriendsCounter}`.yellow);
    console.log('Next cursor => ', `${nextCursor}`.yellow);
    if (remainingReqNum === 0) {
      console.log('The wait has started'.yellow);
      const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
      console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`.yellow);
      setTimeout(this.parse.bind(this), milisecsToWait, { cursor: nextCursor });
      return;
    }

    if (nextCursor !== 0) {
      setTimeout(this.parse.bind(this), 500, { cursor: nextCursor });
    } else {
      await this.updateAndFinish();
    }
  }

  async updateAndFinish() {
    const { screen_name } = this.CurrUser;
    const res = await updateAccount({
      criteria: { screen_name },
      value: { parsed_friendslist: true }
    });
    console.log(res);
    console.log(`All friends' bios of ${screen_name} have been parsed`.bgGreen);
    console.log(`Friends parsed total => ${this.parsedFriendsCounter}`.bgGreen);
    this.emit('finished');
  }
}

async function getAccountToParse() {
  const User = await findOneAccount({ parsed_friendslist: false });
  console.log(`Parsing friendslist of ${User.screen_name}`.bgBlue);
  if (User !== null) {
    const parser = new FriendsListParser(User);
    parser.parse();
    parser.on('finished', getAccountToParse);
  } else {
    console.log('???????');
  }
}
getAccountToParse();
