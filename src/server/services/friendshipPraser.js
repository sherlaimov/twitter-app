import { EventEmitter } from 'events';
import colors from 'colors';
import twitterGet from '../helpers/twitter-get';
import accountRanker from '../helpers/accountRanker';
import { insertManyFriends } from '../db/repositories/mongo/friends';

class FriendshipParser extends EventEmitter {
  constructor(User) {
    super();
    this.CurrUser = User;
    this.isParserRunning = true;
    this.parsedFriendsCounter = 0;
  }

  async parse(_params) {
    const params = {
      screen_name: this.CurrUser.screen_name,
      skip_status: true,
      include_user_entities: false,
      ..._params
    };
    const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
      '/friends/list.json',
      params
    );
    const { users, next_cursor: nextCursor } = data;
    if (users.length === 0) {
      console.log('Returned users array is empty, finishing up...'.green);
      this.finish();
      return;
    }
    this.parsedFriendsCounter += users.length;

    const mappedUsers = users.map(
      ({
        screen_name,
        location,
        description,
        followers_count,
        friends_count,
        verified,
        statuses_count,
        id_str: user_twitter_id
      }) => ({
        user_twitter_id,
        screen_name,
        location,
        description,
        followers_count,
        friends_count,
        verified,
        statuses_count,
        ranking: accountRanker({ followers_count, friends_count, verified, statuses_count }),
        from_friends_list: this.CurrUser._id
      })
    );

    if (mappedUsers.length !== 0) {
      try {
        await insertManyFriends({ friends: mappedUsers, options: { ordered: false } });
        console.log(`*** ==> Inserting ${mappedUsers.length} user accounts <== ***`.red);
      } catch (e) {
        console.log('=> ERROR'.bgRed);
        console.log(e.message);
      }
    }
    if (this.parsedFriendsCounter >= 40) {
      this.emit('received-friends');
      return;
    }

    console.log('Remaining number => ', `${remainingReqNum}`.yellow);
    console.log('Friends parsed total => ', ` ${this.parsedFriendsCounter}`.yellow);
    if (remainingReqNum === 0) {
      console.log('The wait has started'.yellow);
      const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
      console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`.yellow);
      setTimeout(this.parse.bind(this), milisecsToWait, { cursor: nextCursor });
      this.emit('received-friends');
      return;
    }

    if (nextCursor !== 0) {
      setTimeout(this.parse.bind(this), 500, { cursor: nextCursor });
    } else {
      this.finish();
    }
  }

  finish() {
    console.log(`All friends' bios of ${this.CurrUser.screen_name} have been parsed`.bgGreen);
    console.log(`Friends parsed total => ${this.parsedFriendsCounter}`.bgGreen);
    this.emit('finished', this.parsedFriendsCounter);
  }
}

export default FriendshipParser;
