import { EventEmitter } from 'events';
import twitterGet from '../helpers/twitter-get';
import { findFriends } from '../db/repositories/mongo/friends';
import { findOneConnectionAndUpdate } from '../db/repositories/mongo/connections';

class ConnectionsBuilder extends EventEmitter {
  constructor(User) {
    super();
    this.CurrUser = User;
    this.isBuilderRunning = true;
  }
  async getFriendsAndBuild(params) {
    const conditions = {
      condition: { from_friends_list: this.CurrUser._id, ...params },
      projection: 'screen_name',
      options: { limit: 100, skip: 0, sort: { _id: 1 } }
    };
    const friends = await findFriends(conditions);
    if (friends.length === 0 && this.isBuilderRunning === false) {
      console.log('Builder stopped');
      this.emit('connections-built');
      return;
    }
    if (friends.length === 0) {
      console.log('No new friends in the database, waiting...');
      let condition;
      if (this.lastQueriedId === undefined) {
        condition = {};
      } else {
        condition = { _id: { $gt: this.lastQueriedId } };
      }
      setTimeout(this.getFriendsAndBuild.bind(this), 1000, condition);
      return;
    }
    this.lastQueriedId = friends[friends.length - 1];
    this.queryTwitterForConnections(friends);
  }

  async queryTwitterForConnections(screenNames) {
    const screenNamesString = screenNames.map(friend => friend.screen_name).join(',');
    const params = {
      screen_name: screenNamesString
    };
    const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
      '/friendships/lookup.json',
      params
    );
    if (data.length === 0) {
      console.log('Connections have been built');
      return;
    }
    const connections = data.map(
      ({ screen_name, id_str: user_twitter_id, connections: relations }) => ({
        screen_name,
        user_twitter_id,
        relations
      })
    );

    if (connections.length !== 0) {
      try {
        const connectionCond = {
          condition: { screen_name: this.CurrUser.screen_name },
          update: { $push: { connections } },
          options: { upsert: true, new: true }
        };

        await findOneConnectionAndUpdate(connectionCond);
        this.emit('connections-built');
      } catch (e) {
        console.log(e);
      }
    }

    console.log(`Remaining number =>  ${remainingReqNum}`);
    if (remainingReqNum === 0) {
      console.log('The wait has started');
      const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
      console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`);
      setTimeout(this.getFriendsAndBuild.bind(this), milisecsToWait, {
        _id: { $gt: this.lastQueriedId }
      });
    } else {
      this.getFriendsAndBuild({ _id: { $gt: this.lastQueriedId } });
    }
  }
  stopBuilder() {
    console.log('ConnectionsBuilder stopped');
    this.isBuilderRunning = false;
  }
}

export default ConnectionsBuilder;
