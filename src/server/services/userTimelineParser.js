/* eslint no-shadow: ["error", { "allow": ["tweet_time", "tweet_id", "full_text", "retweet_count"] }] */
import events from 'events';
import colors from 'colors';
import twitterGet from '../helpers/twitter-get';
import topicValidator from '../helpers/topic-validator';
import { recordTweetValues, getAnalyzedRetweetValues } from '../helpers/retweet-analyzer';
// import recordTweets from '../db/repositories/sql/tweets';
import { insertManyTweets, findOneAndUpdate } from '../db/repositories/mongo/tweets';
import { createAccountRanking } from '../db/repositories/mongo/accountRetweetRankings';
import { findOneAccount } from '../db/repositories/mongo/accounts';
import downloadTweetMedia from './fileHandler';

const MIN_RETWEET_COUNT = 1000;
const MIN_TRENDING_RETWEET_COUNT = 10000;
const SHOULD_BE_TOPIC_VALIDATED = true;
let lastTweetIdRecord = 0;

const textValidator = topicValidator({ text: true });
/*
 * it collects & stores topic-based tweets AND retweets with MIN_RETWEET_COUNT in FILTERED_TWEETS
 * it calculates the AVG RETWEET Rate & AVG RETWEET Count & stores in account_popularity
*/

class UserTimelineParser extends events.EventEmitter {
  constructor(User) {
    super();
    this.CurrUser = User;
    this.isParserRunning = true;
    this.parsedTweetCount = 0;
    this.on('parser-stop', () => this.stop());
  }
  async parseTimeline(_params) {
    const params = {
      screen_name: this.CurrUser.screen_name,
      trim_user: false,
      include_rts: true,
      exclude_replies: false,
      tweet_mode: 'extended',
      ..._params
    };
    const { remainingReqNum, timeUntilLimitRests, data } = await twitterGet(
      '/statuses/user_timeline.json',
      params
    );

    // Delete the first tweet after the first request
    if (params.max_id) {
      data.shift();
    }

    if (data.length === 0) {
      console.log('Returned data array is empty, finishing up...'.green);
      await this.updateAndFinish();
      return;
    }
    if (this.parsedTweetCount >= 1500) {
      console.log(`Parsed a total of ${this.parsedTweetCount} \n finishing up...`.green);
      await this.updateAndFinish();
      return;
    }
    this.parsedTweetCount += data.length;
    const lastTweetId = data[data.length - 1].id_str;
    lastTweetIdRecord = lastTweetId;

    // filter all tweets to be user own
    const userOwnTweets = data
      .filter(tweet => tweet.retweeted_status === undefined)
      .map(tweet => tweet.retweet_count);

    recordTweetValues(userOwnTweets);

    const filteredTweets = data
      .filter(tweet => {
        if (tweet.retweeted_status) {
          return SHOULD_BE_TOPIC_VALIDATED
            ? tweet.retweeted_status.retweet_count >= MIN_RETWEET_COUNT &&
                textValidator(tweet.retweeted_status.full_text)
            : tweet.retweeted_status.retweet_count >= MIN_RETWEET_COUNT;
        }
        return SHOULD_BE_TOPIC_VALIDATED
          ? tweet.retweet_count >= MIN_RETWEET_COUNT && textValidator(tweet.full_text)
          : tweet.retweet_count >= MIN_RETWEET_COUNT;
      })
      .map(
        ({
          id_str: tweet_id,
          created_at: tweet_time,
          full_text,
          retweeted: retweeted_by_app,
          retweet_count,
          retweeted_status,
          extended_entities
        }) => {
          if (retweeted_status) {
            const {
              created_at: tweet_time,
              id_str: tweet_id,
              full_text,
              retweet_count,
              user,
              extended_entities
            } = retweeted_status;

            return {
              tweet_time,
              tweet_id,
              full_text,
              retweet_count,
              retweeted: true,
              retweeted_by_app,
              screen_name: user.screen_name,
              extended_entities
            };
          }
          return {
            tweet_id,
            tweet_time,
            full_text,
            retweet_count,
            retweeted: false,
            retweeted_by_app,
            screen_name: this.CurrUser.screen_name,
            extended_entities
          };
        }
      );

    if (filteredTweets.length !== 0) {
      try {
        const insertedTweets = await insertManyTweets(filteredTweets); // { ordered: false }
        console.log(`*** ==> Inserting ${filteredTweets.length} tweets <== ***`.red);
        // Filters filteredTweets with insertedTweets tweet_id to find extended_entities and download those
        const insertedWithMedia = filteredTweets
          .filter(targetTweet => {
            const result = insertedTweets.filter(
              insertedTweet => insertedTweet.tweet_id === targetTweet.tweet_id
            );
            return result.length !== 0;
          })
          .filter(tweet => tweet.extended_entities !== undefined);

        if (insertedWithMedia.length !== 0) {
          UserTimelineParser.downloadMediaAndUpdate(insertedWithMedia);
        }
      } catch (e) {
        console.log('===========> ERROR'.bgRed);
        console.log(e.message);
      }
    }

    if (remainingReqNum === 0) {
      console.log('The wait has started'.cyan);
      const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
      console.log(
        `User Timeline Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`.cyan
      );
      setTimeout(this.parseTimeline.bind(this), milisecsToWait, { max_id: lastTweetId });
    } else {
      console.log({ remainingReqNum });
      this.parseTimeline({ max_id: lastTweetId });
    }
  }
  static async downloadMediaAndUpdate(insertedWithMedia) {
    const promises = insertedWithMedia.map(async tweet => {
      const { extended_entities, ...rest } = tweet;
      const fileNames = await downloadTweetMedia({ extended_entities });
      console.log(`${fileNames.length} files downloaded`.green);
      const update = {
        condition: { tweet_id: rest.tweet_id },
        update: { file: fileNames }
      };
      const updated = await findOneAndUpdate(update);
      return updated;
    });
    try {
      const result = await Promise.all(promises);
      return result;
    } catch (e) {
      console.log(e);
    }
  }
  async updateAndFinish() {
    console.log({ lastTweetIdRecord });
    const accountRanking = getAnalyzedRetweetValues();
    accountRanking.screen_name = this.CurrUser.screen_name;
    accountRanking.account = this.CurrUser._id;
    console.log({ accountRanking });
    await this.CurrUser.update({ parsed_timeline: true });
    await createAccountRanking(accountRanking);
    console.log(`The timeline of ${this.CurrUser.screen_name} has been parsed`.bgGreen);
    this.emit('finished');
  }
  stop() {
    this.isParserRunning = false;
  }
}

async function getAccountToParse() {
  const User = await findOneAccount({ parsed_timeline: false });
  console.log(`Parsing timeline of ${User.screen_name}`.bgBlue);
  if (User !== null) {
    const parser = new UserTimelineParser(User);
    parser.parseTimeline();
    parser.on('finished', getAccountToParse);
  } else {
    console.log('All available account timelines have been successfully parsed');
  }
}

getAccountToParse();
