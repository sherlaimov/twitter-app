import fs from 'fs';
import fetch from 'node-fetch';
import uuid from 'uuid/v4';
import config from '../config';

const { mediaDir } = config;

function downloadFile(response) {
  const [, fileExt] = response.headers.get('content-type').split('/');
  const { body } = response;
  const fileName = uuid();
  return new Promise((resolve, reject) => {
    const dest = fs.createWriteStream(`${mediaDir}/${fileName}.${fileExt}`);
    body.pipe(dest);
    body.on('end', () => {});
    body.on('error', err => reject(err));
    dest.on('finish', () => resolve(`${fileName}.${fileExt}`));
    dest.on('error', err => reject(err));
  });
}
const downloadTweetMedia = async ({ extended_entities: entities }) => {
  const { media } = entities;
  const [firstMedia] = media;
  if (firstMedia.type === 'video' || firstMedia.type === 'animated_gif') {
    const { variants } = firstMedia.video_info;
    const results = variants
      .filter(item => item.bitrate !== undefined)
      .sort((a, b) => a.bitrate - b.bitrate);
    const { url } = results[results.length - 1];
    const fileName = await fetch(url).then(res => downloadFile(res));
    return [fileName];
  }
  const promises = media.map(({ media_url_https: url }) =>
    fetch(url).then(res => downloadFile(res))
  );
  const fileNames = await Promise.all(promises);

  return fileNames;
};

export default downloadTweetMedia;
