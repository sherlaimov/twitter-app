export class NotFoundError extends Error {
  constructor() {
    super('Not founddd');
    this.status = 404;
    this.expose = true;
  }
}

export default entity => {
  if (!entity) {
    throw new NotFoundError();
  }
  return entity;
};
