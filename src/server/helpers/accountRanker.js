const followerRanges = [
  [5000, 10000, 2],
  [10000, 20000, 3],
  [20000, 30000, 4],
  [30000, 40000, 5],
  [40000, 50000, 6],
  [50000, 60000, 7],
  [60000, 70000, 8],
  [70000, 80000, 9],
  [80000, 90000, 10]
];

const isInRange = (val, min, max) => val >= min && val < max;

function getFollowersRanking(followersCount) {
  if (followersCount < followerRanges[0][0]) {
    return 1;
  }
  if (followersCount > followerRanges[followerRanges.length - 1][1]) {
    return 11;
  }

  const field = followerRanges.find(pair => isInRange(followersCount, pair[0], pair[1]));
  return field[2];
}

function accountRanker({
  followers_count: followersCount,
  friends_count: friendsCount,
  verified,
  statuses_count: tweetCount
}) {
  let followersFriendsDiff = 0.5;
  let verifiedScore = 0;
  if (verified) verifiedScore = 0.5;

  const userFollowerRanking = getFollowersRanking(followersCount);

  if (userFollowerRanking === 1 && followersCount - friendsCount <= 1000) {
    followersFriendsDiff = 0;
  }
  return followersFriendsDiff + userFollowerRanking + verifiedScore;
}
export default accountRanker;
