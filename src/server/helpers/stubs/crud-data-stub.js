export const account = {
  id_str: '263744389',
  name: 'Eugene Sherlaimov',
  screen_name: 'sherlaimov',
  location: 'Kharkov',
  profile_location: null,
  description:
    'Cosmopolitan. Aspiring Web Developer & Entrepreneur. Rollerskater. Atheist. Business Analyst @Truver_Inc. Originally from @Sloviansk (Tweeting in ENG & RU)',
  url: 'http://t.co/KNowrfXtUE',
  protected: false,
  followers_count: 650000,
  friends_count: 945,
  listed_count: 53,
  created_at: 'Thu Mar 10 17:15:05 +0000 2011',
  verified: true
};

const testTweet = {
  tweet_id: '1017723667193761792',
  screen_name: 'iamdevloper',
  full_text: '"Oh dear oh dear, when people Reduce themselves to these sorts of Actions.',
  retweet_count: 33,
  tweet_time: 'Fri Jul 13 10:53:25 +0000 2018',
  retweeted: true,
  used: false
};

export const tweets = [
  {
    created_at: 'Tue Jul 24 16:22:27 +0000 2018',
    id: 1021792736482152454,
    id_str: '1021792736482152454',
    full_text:
      'One sure way to ruin your child\'s online life forever:\n\nName them "[object Object]"',
    truncated: false,
    display_text_range: [0, 83],
    entities: {
      hashtags: [],
      symbols: [],
      user_mentions: [],
      urls: []
    },
    source: '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>',
    in_reply_to_status_id: null,
    in_reply_to_status_id_str: null,
    in_reply_to_user_id: null,
    in_reply_to_user_id_str: null,
    in_reply_to_screen_name: null,
    user: {
      id: 564919357,
      id_str: '564919357'
    },
    geo: null,
    coordinates: null,
    place: null,
    contributors: null,
    is_quote_status: false,
    retweet_count: 358,
    favorite_count: 1928,
    favorited: false,
    retweeted: false,
    lang: 'en'
  },
  {
    created_at: 'Wed Jul 25 10:16:18 +0000 2018',
    id: 1022062979699879936,
    id_str: '1022062979699879936',
    full_text:
      'RT @Honest_Work: To the person that read the tweet below and thought it was a good idea to ping our site using IE6, thank you for the early…',
    truncated: false,
    display_text_range: [0, 140],
    entities: {
      hashtags: [],
      symbols: [],
      user_mentions: [
        {
          screen_name: 'Honest_Work',
          name: 'Honest Work',
          id: 966641692853010433,
          id_str: '966641692853010433',
          indices: [3, 15]
        }
      ],
      urls: []
    },
    source: '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>',
    in_reply_to_status_id: null,
    in_reply_to_status_id_str: null,
    in_reply_to_user_id: null,
    in_reply_to_user_id_str: null,
    in_reply_to_screen_name: null,
    user: {
      id: 564919357,
      id_str: '564919357'
    },
    geo: null,
    coordinates: null,
    place: null,
    contributors: null,
    retweeted_status: {
      created_at: 'Wed Jul 25 10:08:02 +0000 2018',
      id: 1022060899429965824,
      id_str: '1022060899429965824',
      full_text:
        'To the person that read the tweet below and thought it was a good idea to ping our site using IE6, thank you for the early morning panic attack. \n\nhttps://t.co/hitlHzZobq',
      truncated: false,
      display_text_range: [0, 170],
      entities: {
        hashtags: [],
        symbols: [],
        user_mentions: [],
        urls: [
          {
            url: 'https://t.co/hitlHzZobq',
            expanded_url: 'https://twitter.com/iamdevloper/status/1021695779356983299?s=21',
            display_url: 'twitter.com/iamdevloper/st…',
            indices: [147, 170]
          }
        ]
      },
      source: '<a href="https://buffer.com" rel="nofollow">Buffer</a>',
      in_reply_to_status_id: null,
      in_reply_to_status_id_str: null,
      in_reply_to_user_id: null,
      in_reply_to_user_id_str: null,
      in_reply_to_screen_name: null,
      user: {
        id: 966641692853010433,
        id_str: '966641692853010433'
      },
      geo: null,
      coordinates: null,
      place: null,
      contributors: null,
      is_quote_status: true,
      quoted_status_id: 1021695779356983299,
      quoted_status_id_str: '1021695779356983299',
      quoted_status_permalink: {
        url: 'https://t.co/hitlHzZobq',
        expanded: 'https://twitter.com/iamdevloper/status/1021695779356983299?s=21',
        display: 'twitter.com/iamdevloper/st…'
      },
      quoted_status: {
        created_at: 'Tue Jul 24 09:57:10 +0000 2018',
        id: 1021695779356983299,
        id_str: '1021695779356983299',
        full_text:
          "Every now and then, ping one of your competitor's websites using an IE6 VM. Keep them on their toes.",
        truncated: false,
        display_text_range: [0, 100],
        entities: {
          hashtags: [],
          symbols: [],
          user_mentions: [],
          urls: []
        },
        source: '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>',
        in_reply_to_status_id: null,
        in_reply_to_status_id_str: null,
        in_reply_to_user_id: null,
        in_reply_to_user_id_str: null,
        in_reply_to_screen_name: null,
        user: {
          id: 564919357,
          id_str: '564919357'
        },
        geo: null,
        coordinates: null,
        place: null,
        contributors: null,
        is_quote_status: false,
        retweet_count: 3076,
        favorite_count: 9861,
        favorited: false,
        retweeted: false,
        lang: 'en'
      },
      retweet_count: 159,
      favorite_count: 968,
      favorited: false,
      retweeted: false,
      possibly_sensitive: false,
      lang: 'en'
    },
    is_quote_status: true,
    quoted_status_id: 1021695779356983299,
    quoted_status_id_str: '1021695779356983299',
    quoted_status_permalink: {
      url: 'https://t.co/hitlHzZobq',
      expanded: 'https://twitter.com/iamdevloper/status/1021695779356983299?s=21',
      display: 'twitter.com/iamdevloper/st…'
    },
    retweet_count: 159,
    favorite_count: 0,
    favorited: false,
    retweeted: false,
    lang: 'en'
  }
];

const userOwnTweets = tweets
  .filter(tweet => tweet.retweeted_status === undefined)
  .map(tweet => tweet.retweet_count);

// const testData = [{ a: 1 }, { a: 2 }, { a: 3 }];

// testData.map(obj => {
//   if (obj.a === 2) {
//     obj.test = 'OUGH!';
//   }
//   return obj;
// });

console.log(userOwnTweets);
