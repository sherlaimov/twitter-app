let allTweetsCount = 0;
let allTweetsWithRetweetsCount = 0;
const retweetValues = [];

const testArr = [3, 4, 1, 5, 3, 6, 8, 1000, 345, 23, 5, 3, 2, 3, 4];
const arrAvg = arr => arr.reduce((a, b) => a + b, 0) / arr.length;
function getMedian(array) {
  if (!array.length) {
    return 0;
  }
  const numbers = array.slice(0).sort((a, b) => a - b);
  const middle = Math.floor(numbers.length / 2);
  const isEven = numbers.length % 2 === 0;
  return isEven ? (numbers[middle] + numbers[middle - 1]) / 2 : numbers[middle];
}

function getTweetToRetweetRatio() {
  return parseFloat((allTweetsWithRetweetsCount / allTweetsCount).toFixed(2));
}

export function recordTweetValues(retweetValsArr) {
  allTweetsCount += retweetValsArr.length;
  const withRetweets = retweetValsArr.filter(val => val > 0);
  allTweetsWithRetweetsCount += withRetweets.length;
  withRetweets.forEach(val => retweetValues.push(val));
}

export function getAnalyzedRetweetValues() {
  return {
    tweetToRetweetRatio: getTweetToRetweetRatio(),
    avgRetweetCount: getMedian(retweetValues)
  };
}
