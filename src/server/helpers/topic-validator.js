import { profileTargetWords, tweetTargetWords } from '../db/target-words';

const targetWords = [...profileTargetWords];

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

const topicValidator = config => {
  if (config.text) {
    targetWords.push(...tweetTargetWords);
  }
  return _text => {
    let stringArray;
    if (_text.includes('\n')) {
      stringArray = _text
        .split('\n')
        .join(' ')
        .split(' ');
    } else {
      stringArray = _text.split(' ');
    }
    return stringArray.some(word =>
      targetWords.some(target =>
        // const escapedTarget = escapeRegExp(target);
        // const regExBeginning = `^(\n|#|@|"|'|\`|‘|“|~)?`;
        // const test = new RegExp(`${regExBeginning}${escapedTarget}(s|es|\'s|ing)?`, 'i');
        // return test.test(word);
        word.toLowerCase().includes(target.toLowerCase())
      )
    );
  };
};

export default topicValidator;
