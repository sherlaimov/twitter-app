import moment from 'moment';

export default function getTimeDiffFromString({ twitterTimeStr, value }) {
  const lastParsedTweetTime = moment(twitterTimeStr, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en');
  const timeDiffAbs = Math.abs(moment.duration(lastParsedTweetTime.diff(moment())).as(value));
  // console.log(lastParsedTweetTime.format('LLL'));
  return timeDiffAbs;
}
