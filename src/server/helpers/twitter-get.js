import Twitter from 'twitter';
import config from '../config';

const twitterGet = async (route, _params) => {
  const twitterKeys = {
    consumer_key: process.env.TWI_CONSUMER_KEY,
    consumer_secret: process.env.TWI_CONSUMER_SECRET,
    access_token_key: process.env.TWI_ACCESS_TOKEN_KEY,
    access_token_secret: process.env.TWI_ACCESS_TOKEN_SECRET
  };
  const twitter = new Twitter(twitterKeys);
  const params = {
    ..._params
  };
  const promise = new Promise((resolve, reject) => {
    twitter.get(route, params, (error, data, response) => {
      if (error) reject(error);
      const { headers } = response;
      const result = {
        data,
        remainingReqNum: parseInt(headers['x-rate-limit-remaining'], 10),
        timeUntilLimitRests: parseInt(headers['x-rate-limit-reset'], 10)
      };
      resolve(result);
    });
  });
  try {
    const result = await promise;
    return result;
  } catch (e) {
    // TODO How to properly handle such errors?
    // [ { message: 'Rate limit exceeded', code: 88 } ]
    console.log('==============> ERROR IN twitterGet');
    console.log(e);
    throw new Error(e);
  }
};

export default twitterGet;
