import Router from 'koa-router';
import { findConnections } from '../db/repositories/mongo/connections';

const router = new Router();

router.get('/:user', async ctx => {
  const { user } = ctx.params;
  const conditions = {
    condition: { screen_name: user }
  };
  const [data] = await findConnections(conditions);
  const { connections, screen_name } = data;
  const mappedConnections = connections.map(({ screen_name, relations }) => ({
    screen_name,
    relations
  }));
  ctx.body = { connections: mappedConnections, screen_name };
});

export default router;
