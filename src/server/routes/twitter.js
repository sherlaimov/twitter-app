import Router from 'koa-router';
import jwtMiddleware from 'koa-jwt';
import { findOneUser } from '../db/repositories/mongo/users';
import { findOneConnection } from '../db/repositories/mongo/connections';
import FriendshipParser from '../services/friendshipPraser';
import ConnectionsBuilder from '../services/connectionsBuilder';
import config from '../config';

// TODO configure the app to work with dotenv properly
const router = new Router();

const mapConnectionsData = data => {
  const { connections } = data;
  const mappedConnections = connections.map(({ screen_name, relations }) => ({
    screen_name,
    relations
  }));
  return mappedConnections;
};
router.use(jwtMiddleware({ secret: config.secretKey }));
router.use((ctx, next) => {
  if (!ctx.state.user) {
    ctx.throw(401, 'Authorizaton required');
  }
  return next();
});
router.get('/graph/:username', async ctx => {
  const { username } = ctx.params;
  const conditions = { condition: { screen_name: username } };
  const existingConnections = await findOneConnection(conditions);
  if (existingConnections !== null) {
    ctx.body = { connections: mapConnectionsData(existingConnections), screen_name: username };
    return;
  }
  const User = await findOneUser(conditions);
  const friendshipParser = new FriendshipParser(User);
  const conBuilder = new ConnectionsBuilder(User);
  friendshipParser.parse();
  conBuilder.getFriendsAndBuild();

  let resolve1;
  const friendshipPromise = new Promise((ok, reject) => (resolve1 = ok));
  let resolve2;
  const connectionsPromise = new Promise((ok, reject) => (resolve2 = ok));

  friendshipParser.on('received-friends', () => {
    conBuilder.stopBuilder();
    resolve1();
  });
  await friendshipPromise;

  conBuilder.on('connections-built', () => {
    console.log('connections-built event fired');
    resolve2();
  });
  await connectionsPromise;

  const data = await findOneConnection(conditions);
  ctx.body = { connections: mapConnectionsData(data), screen_name: username };
});

export default router;
