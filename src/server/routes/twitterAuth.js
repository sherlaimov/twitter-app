import Router from 'koa-router';
import passport from 'koa-passport';
import jwt from 'jsonwebtoken';
import fetch from 'node-fetch';
import crypto from 'crypto';
import dotenv from 'dotenv';
import OAuth from 'oauth-1.0a';
import config from '../config';

dotenv.config({ path: 'variables.env' });

const router = new Router();

function hashFunctionSha1(baseString, key) {
  return crypto
    .createHmac('sha1', key)
    .update(baseString)
    .digest('base64');
}
const oauthOpts = {
  consumer: { key: process.env.TWI_CONSUMER_KEY, secret: process.env.TWI_CONSUMER_SECRET },
  signature_method: 'HMAC-SHA1',
  hash_function: hashFunctionSha1
};
const oauth = OAuth(oauthOpts);

const createToken = auth =>
  jwt.sign(
    {
      id: auth.id,
      screenName: auth.screenName
    },
    config.secretKey,
    {
      expiresIn: 60 * 120
    }
  );

const generateToken = async (ctx, next) => {
  ctx.request.token = createToken(ctx.request.auth);
  await next();
};

const sendToken = async ctx => {
  ctx.set('x-auth-token', ctx.request.token);
  ctx.status = 200;
  ctx.body = JSON.stringify(ctx.state.user);
};

router.post('/twitter/reverse', async ctx => {
  const encodedCb = encodeURIComponent('http://localhost:3000/auth/twitter-callback');
  const url = `https://api.twitter.com/oauth/request_token?oauth_callback=${encodedCb}`;
  const requestData = {
    url,
    method: 'POST'
  };

  try {
    const JSONedResponse = await fetch(url, {
      method: 'POST',
      headers: oauth.toHeader(oauth.authorize(requestData))
    })
      .then(res => {
        console.log(res.status);
        if (res.ok !== true) throw new Error(res.statusText);
        return res.buffer();
      })
      .then(data => {
        const jsonStr = `{ "${data
          .toString()
          .replace(/&/g, '", "')
          .replace(/=/g, '": "')}"}`;
        return jsonStr;
      });
    ctx.set('Content-Type', 'application/json');
    ctx.body = JSONedResponse;
  } catch (e) {
    ctx.throw(500, e.message);
  }
});

router.post(
  '/twitter-callback',
  async (ctx, next) => {
    const { oauth_token: oauthToken, oauth_verifier: oauthVerifier } = ctx.query;
    const token = { key: oauthToken };
    const url = `https://api.twitter.com/oauth/access_token?oauth_verifier=${oauthVerifier}`;
    const requestData = {
      url,
      method: 'POST'
    };
    try {
      await fetch(url, {
        method: 'POST',
        headers: oauth.toHeader(oauth.authorize(requestData, token))
      })
        .then(res => {
          if (res.ok !== true) throw new Error(res.statusText);
          return res.buffer();
        })
        .then(body => {
          const bodyString = `{ "${body
            .toString()
            .replace(/&/g, '", "')
            .replace(/=/g, '": "')}"}`;
          const { oauth_token, oauth_token_secret, user_id, screen_name } = JSON.parse(bodyString);
          ctx.request.body.oauth_token = oauth_token;
          ctx.request.body.oauth_token_secret = oauth_token_secret;
          ctx.request.body.user_id = user_id;
          ctx.request.body.screen_name = screen_name;
          process.env.TWI_ACCESS_TOKEN_KEY = oauth_token;
          process.env.TWI_ACCESS_TOKEN_SECRET = oauth_token_secret;
        });
      await next();
    } catch (e) {
      ctx.throw(500, e.message);
    }
  },
  passport.authenticate('twitter-token', { session: false }),
  async (ctx, next) => {
    if (ctx.state.user === undefined) {
      ctx.throw(401, 'User not authenticated');
    }
    const { _id: id, screen_name: screenName } = ctx.state.user;
    ctx.request.auth = {
      id,
      screenName
    };
    await next();
  },
  generateToken,
  sendToken
);

export default router;
