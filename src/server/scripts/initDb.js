import db from '../db/sql';

const DROP_ALL_TABLES = true;

async function hasTable(tableName) {
  const res = await db.schema.hasTable(tableName);
  return res;
}

async function initDb() {
  if (DROP_ALL_TABLES) {
    // await db.schema.dropTableIfExists('tweets');
    await db.schema.dropTableIfExists('filtered_tweets');
  }
  try {
    if (!(await db.schema.hasTable('filtered_tweets'))) {
      await db.schema.createTable('filtered_tweets', t => {
        t.increments().primary();
        t.text('tweet_id');
        t.text('full_text');
        t.integer('retweet_count');
        t.text('tweet_time');
        t.string('screen_name');
        t.boolean('retweeted');
        t.boolean('used').defaultTo(false);
        t.timestamps(false, true);
        // t.timestamp('created_at').defaultTo(db.fn.now());
        // t.timestamp('updated_at').defaultTo(
        //   db.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        // );
      });
    }

    if (!(await db.schema.hasTable('tweets'))) {
      await db.schema.createTable('tweets', t => {
        t.increments().primary();
        t.string('user_twitter_id');
        t.string('screen_name');
        t.text('tweet_text');
        t.integer('retweet_count');
        t.integer('favorite_count');
        t.boolean('used').defaultTo(false);
        t.text('tweet_id');
        t.text('tweet_time');
        t.timestamps(true);
      });
    }
    if (!(await db.schema.hasTable('filtered_users'))) {
      await db.schema.createTable('filtered_users', t => {
        t.increments().primary();
        t.string('user_twitter_id')
          .unique()
          .notNullable();
        t.string('screen_name')
          .unique()
          .notNullable();
        t.text('description');
        t.integer('followers_count');
        t.integer('friends_count');
        t.text('location');
        t.boolean('verified');
        t.integer('statuses_count');
        t.integer('ranking');
        t.timestamp('created_at').defaultTo(db.fn.now());
      });
    }
  } catch (e) {
    console.log('asdasd');
    console.log(e);
  }
  await db.destroy();
}

initDb();
