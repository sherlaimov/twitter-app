import twitterGet from '../helpers/twitter-get';
import accountRanker from '../helpers/accountRanker';
import { insertManyFriends } from '../db/repositories/mongo/friendsLists';

// ! CHANGE THE HARD-CODED ID ! AND REMOVE CURSOR
// ? Enable user to set MIN_FOLLOWERS_COUNT for outgoing friendships?
// TODO Make sure you do not hard-code passing of _id for reference
// TODO last cursor - 1474163809815647200
let parsedFriendsCounter = 0;

const friendsListParser = async _params => {
  const params = {
    screen_name: 'sherlaimov',
    skip_status: true,
    include_user_entities: false,
    cursor: '1474163809815647200',
    ..._params
  };
  const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
    '/friends/list.json',
    params
  );
  const { users, next_cursor: nextCursor } = data;
  if (users.length === 0) {
    console.log('Returned users array is empty');
    console.log(`Friends parsed total => ${parsedFriendsCounter}`);
    return;
  }
  parsedFriendsCounter += users.length;
  const mappedUsers = users.map(
    ({
      screen_name,
      location,
      description,
      followers_count,
      friends_count,
      verified,
      statuses_count,
      id_str: user_twitter_id
    }) => ({
      user_twitter_id,
      screen_name,
      location,
      description,
      followers_count,
      friends_count,
      verified,
      statuses_count,
      ranking: accountRanker({ followers_count, friends_count, verified, statuses_count }),
      from_friends_list: '5b6211863fea6122e3b5b444'
    })
  );
  if (mappedUsers.length !== 0) {
    try {
      const inserted = await insertManyFriends({ friends: mappedUsers });
      console.log(`*** ==> Inserting ${inserted.length} user accounts <== ***`);
    } catch (e) {
      console.log('===========> ERROR');
      console.log(e.message);
    }
  }
  console.log(`Remaining number =>  ${remainingReqNum}`);
  console.log(`Friends parsed total => ${parsedFriendsCounter}`);
  console.log(`Next cursor =>  ${nextCursor}`);
  if (remainingReqNum === 0) {
    console.log('The wait has started');
    const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
    console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`);
    setTimeout(friendsListParser, milisecsToWait, { cursor: nextCursor });
    return;
  }

  if (nextCursor !== 0) {
    setTimeout(friendsListParser, 500, { cursor: nextCursor });
  } else {
    console.log(`All friends' bios of ${params.screen_name} have been parsed`);
    console.log(`Friends parsed total => ${parsedFriendsCounter}`);
  }
};

friendsListParser({});
