/* eslint no-shadow: ["error", { "allow": ["tweet_time", "tweet_id", "full_text", "retweet_count"] }] */
import twitterGet from '../helpers/twitter-get';
import topicValidator from '../helpers/topic-validator';
import { recordTweetValues, getAnalyzedRetweetValues } from '../helpers/retweet-analyzer';
// import recordTweets from '../db/repositories/sql/tweets';
import insertManyTweets from '../db/repositories/mongo/tweets';
import {
  createAccountRanking,
  findOneRetweetRanking
} from '../db/repositories/mongo/accountRetweetRankings';
import { findOneAccount } from '../db/repositories/mongo/accounts';

const MIN_RETWEET_COUNT = 500;
const SHOULD_BE_TOPIC_VALIDATED = true;
let lastTweetIdRecord = 0;
let currUserId = null;
// if there is a record in account_retweet_ranking then
// account has been parsed
/*
 * it collects & stores topic-based tweets AND retweets with MIN_RETWEET_COUNT in FILTERED_TWEETS
 * it calculates the AVG RETWEET Rate & AVG RETWEET Count & stores in account_popularity
*/

const userTimelineParser = async _params => {
  const params = {
    screen_name: 'dan_abramov',
    trim_user: false,
    include_rts: true,
    exclude_replies: false,
    tweet_mode: 'extended',
    ..._params
  };
  const { remainingReqNum, timeUntilLimitRests, data } = await twitterGet(
    '/statuses/user_timeline.json',
    params
  );

  // Delete the first tweet after the first request
  if (params.max_id) {
    data.shift();
  }

  if (data.length === 0) {
    console.log('Returned data array is empty');
    console.log({ lastTweetIdRecord });
    const retweetValues = getAnalyzedRetweetValues();
    retweetValues.screen_name = params.screen_name;
    console.log({ retweetValues });
    // TODO SET ACCOUNTS parsed TO TRUE
    await createAccountRanking(retweetValues);
    return;
  }

  const lastTweetId = data[data.length - 1].id_str;
  lastTweetIdRecord = lastTweetId;

  // filter all tweets to be user own
  const userOwnTweets = data
    .filter(tweet => tweet.retweeted !== true)
    .map(tweet => tweet.retweet_count);

  recordTweetValues(userOwnTweets);

  const filteredTweets = data
    .filter(tweet => {
      if (tweet.retweeted) {
        return SHOULD_BE_TOPIC_VALIDATED
          ? tweet.retweeted_status.retweet_count >= MIN_RETWEET_COUNT &&
              topicValidator(tweet.retweeted_status.full_text)
          : tweet.retweeted_status.retweet_count >= MIN_RETWEET_COUNT;
      }
      return SHOULD_BE_TOPIC_VALIDATED
        ? tweet.retweet_count >= MIN_RETWEET_COUNT && topicValidator(tweet.full_text)
        : tweet.retweet_count >= MIN_RETWEET_COUNT;
    })
    .map(
      ({
        id_str: tweet_id,
        created_at: tweet_time,
        full_text,
        retweeted: retweeted_by_app,
        retweet_count,
        retweeted_status
      }) => {
        if (retweeted_status) {
          const {
            created_at: tweet_time,
            id_str: tweet_id,
            full_text,
            retweet_count,
            user
          } = retweeted_status;

          return {
            tweet_time,
            tweet_id,
            full_text,
            retweet_count,
            retweeted: true,
            retweeted_by_app,
            screen_name: user.screen_name
          };
        }
        return {
          tweet_id,
          tweet_time,
          full_text,
          retweet_count,
          retweeted: false,
          retweeted_by_app,
          screen_name: params.screen_name
        };
      }
    );
  try {
    if (filteredTweets.length !== 0) {
      await insertManyTweets(filteredTweets);
      console.log(`*** ==> Inserting ${filteredTweets.length} tweets <== ***`);
    }
  } catch (e) {
    console.log('===========> ERROR');
    console.log(e);
  }

  if (remainingReqNum === 0) {
    console.log('The wait has started');
    const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 1000;
    console.log(`User Timeline Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`);
    setTimeout(userTimelineParser, milisecsToWait, { max_id: lastTweetId });
  } else {
    console.log({ remainingReqNum });
    userTimelineParser({ max_id: lastTweetId });
  }
};

async function getAccountToParse() {
  const accToParse = await findOneAccount({ parsed_timeline: false });
  currUserId = accToParse._id;
  if (accToParse !== null) {
    // userTimelineParser({ screen_name: accToParse.screen_name });
  }
}
getAccountToParse();
// userTimelineParser({});
