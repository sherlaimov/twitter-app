import { findTweetsByCondition } from '../db/repositories/mongo/tweets';
import { findAccountsByCondition } from '../db/repositories/mongo/accounts';
import topicValidator from '../helpers/topic-validator';

const validator = topicValidator({ text: false });

async function testValidator() {
  const conditions = {
    condition: {},
    projection: 'full_text screen_name',
    options: null
  };
  const data = await findTweetsByCondition(conditions);
  // const data = await findAccountsByCondition(conditions);
  console.log(data.length);
  const filtered = data.filter(({ full_text: text }, i) => {
    console.log('Processing field', i);
    return validator(text) === false;
  });
  console.log(filtered);
  // filtered.forEach((query, i) => {
  //   console.log('removing query', i);
  //   query.remove();
  // });
}
const test = 'She’s tough. But she’s worth it https://t.co/iBg7XMQdhL';
validator(test); // ?
// testValidator();
