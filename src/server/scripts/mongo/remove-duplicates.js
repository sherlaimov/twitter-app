import Tweet from '../../db/models/tweet';

async function findAndRemoveDuplicates() {
  const result = await Tweet.aggregate([
    {
      $group: {
        _id: { tweet_id: '$tweet_id' },
        dups: { $addToSet: '$_id' },
        count: { $sum: 1 }
      }
    },
    {
      $match: {
        count: { $gt: 1 }
      }
    },
    { $sort: { count: -1 } }
  ]);

  // https://blog.lavrton.com/javascript-loops-how-to-handle-async-await-6252dd3c795
  for (const doc of result) {
    doc.dups.shift();
    const removed = await Tweet.remove({ _id: { $in: doc.dups } });
    console.log(removed);
  }
}

// findAndRemoveDuplicates();
