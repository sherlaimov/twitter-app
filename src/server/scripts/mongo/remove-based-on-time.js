import { findTweets } from '../../db/repositories/mongo/tweets';

async function removeBasedOnTime() {
  const conditions = {
    condition: {
      createdAt: {
        // (7 * 24 * 60 * 60 * 1000) is 7 days of millis
        $gte: new Date(new Date().getTime() - 5 * 24 * 60 * 60 * 1000)
      }
    }
  };
  const tweets = await findTweets(conditions);
  const promises = tweets.map(async tweet => {
    const removed = await tweet.remove();
    return removed;
  });
  const result = await Promise.all(promises);
  console.log(result.length);
}

removeBasedOnTime();
