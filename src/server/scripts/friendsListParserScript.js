import twitterGet from '../helpers/twitter-get';
import topicValidator from '../helpers/topic-validator';
import accountRanker from '../helpers/accountRanker';
// import insertNewUsers from '../db/repositories/sql/users';
import {
  insertManyAccounts,
  createAccount,
  updateAccount
} from '../db/repositories/mongo/accounts';

const MIN_FOLLOWERS_COUNT = 2000;
let parsedFriendsCounter = 0;
// ? Should I store the last requested cursor?

const createInitialAccount = async _params => {
  const params = {
    screen_name: 'alicegoldfuss',
    skip_status: true,
    include_user_entities: false,
    ..._params
  };
  const { data } = await twitterGet('/users/show.json', params);
  const {
    screen_name,
    location,
    description,
    followers_count,
    friends_count,
    verified,
    statuses_count,
    id_str: user_twitter_id
  } = data;
  const ranking = accountRanker({ followers_count, friends_count, verified, statuses_count });
  const result = await createAccount({
    screen_name,
    location,
    description,
    followers_count,
    friends_count,
    verified,
    statuses_count,
    user_twitter_id,
    ranking
  });
  console.log(result);
};
// createInitialAccount({});
const friendsListParser = async _params => {
  const params = {
    screen_name: 'alicegoldfuss',
    skip_status: true,
    include_user_entities: false,
    ..._params
  };
  const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
    '/friends/list.json',
    params
  );
  const { users, next_cursor: nextCursor } = data;
  if (users.length === 0) {
    console.log('Returned users array is empty');
    console.log(`Friends parsed total => ${parsedFriendsCounter}`);
    return;
  }
  parsedFriendsCounter += users.length;
  const filteredUsers = users
    .filter(user => user.followers_count >= MIN_FOLLOWERS_COUNT && topicValidator(user.description))
    .map(
      ({
        screen_name,
        location,
        description,
        followers_count,
        friends_count,
        verified,
        statuses_count,
        id_str: user_twitter_id
      }) => ({
        user_twitter_id,
        screen_name,
        location,
        description,
        followers_count,
        friends_count,
        verified,
        statuses_count,
        ranking: accountRanker({ followers_count, friends_count, verified, statuses_count })
      })
    );

  try {
    if (filteredUsers.length !== 0) {
      await insertManyAccounts(filteredUsers);
      console.log(`*** ==> Inserting ${filteredUsers.length} user accounts <== ***`);
    }
  } catch (e) {
    console.log('===========> ERROR');
    console.log(e.message);
    // console.log(filteredUsers);
    // return;
  }
  console.log(`Remaining number =>  ${remainingReqNum}`);
  console.log(`Friends parsed total => ${parsedFriendsCounter}`);
  console.log(`Next cursor =>  ${nextCursor}`);
  if (remainingReqNum === 0) {
    console.log('The wait has started');
    const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 1000;
    console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`);
    setTimeout(friendsListParser, milisecsToWait, { cursor: nextCursor });
    return;
  }

  if (nextCursor !== 0) {
    setTimeout(friendsListParser, 500, { cursor: nextCursor });
  } else {
    const { screen_name } = params;
    await updateAccount({
      criteria: { screen_name },
      value: { parsed_friendslist: true }
    });
    console.log(`All friends' bios of ${screen_name} have been parsed`);
    console.log(`Friends parsed total => ${parsedFriendsCounter}`);
  }
};

friendsListParser({});

// x-rate-limit-limit →15
// x-rate-limit-remaining →7
// x-rate-limit-reset →1530822448
