import minimist from 'minimist';
import twitterGet from '../helpers/twitter-get';
import accountRanker from '../helpers/accountRanker';
import { createAccount } from '../db/repositories/mongo/accounts';
import { createFriend } from '../db/repositories/mongo/friendsLists';

/* 
  USAGE:
   npm run init:account --  --screen_name <name>
*/

const args = minimist(process.argv.slice(2), {
  string: 'screen_name'
});

const { screen_name } = args;

const createInitialAccount = async _params => {
  const params = {
    skip_status: true,
    include_user_entities: false,
    ..._params
  };
  const { data } = await twitterGet('/users/show.json', params);

  const {
    screen_name,
    location,
    description,
    followers_count,
    friends_count,
    verified,
    statuses_count,
    id_str: user_twitter_id
  } = data;
  const ranking = accountRanker({ followers_count, friends_count, verified, statuses_count });
  try {
    const result = await createFriend({
      screen_name,
      location,
      description,
      followers_count,
      friends_count,
      verified,
      statuses_count,
      user_twitter_id,
      ranking
    });
    console.log(result);
  } catch (e) {
    throw new Error(e);
  }
};

createInitialAccount({ screen_name });
