import twitterGet from '../helpers/twitter-get';
import { findFriends, findOneFriend } from '../db/repositories/mongo/friends';
import { fineOneConnectionAndUpdate } from '../db/repositories/mongo/connections';

// ? https://stackoverflow.com/questions/5539955/how-to-paginate-with-mongoose-in-node-js/23640287#23640287
let targetUser;
const connectionsBuilder = async _params => {
  const conditions = {
    condition: { from_friends_list: targetUser._id },
    projection: 'screen_name',
    options: { limit: 100, skip: 0 },
    ..._params
  };
  const friends = await findFriends(conditions);
  const screenNames = friends.map(friend => friend.screen_name).join(',');

  const params = {
    screen_name: screenNames
  };
  const { data, remainingReqNum, timeUntilLimitRests } = await twitterGet(
    '/friendships/lookup.json',
    params
  );
  if (data.length === 0) {
    console.log('Connections have been built');
    return;
  }
  const connections = data.map(
    ({ screen_name, id_str: user_twitter_id, connections: relations }) => ({
      screen_name,
      user_twitter_id,
      relations
    })
  );

  if (connections.length !== 0) {
    try {
      const connectionCond = {
        condition: { screen_name: targetUser.screen_name },
        update: { $push: { connections } },
        options: { upsert: true, new: true }
      };

      const result = await fineOneConnectionAndUpdate(connectionCond);
      console.log(result);
    } catch (e) {
      console.log(e);
    }
  }
  const { skip } = conditions.options;
  const newSkip = skip + 100;
  console.log(`Remaining number =>  ${remainingReqNum}`);
  if (remainingReqNum === 0) {
    console.log('The wait has started');
    const milisecsToWait = (timeUntilLimitRests - Math.round(Date.now() / 1000)) * 1000 + 2000;
    console.log(`Parser will continue at ${new Date(timeUntilLimitRests * 1000)}`);
    setTimeout(connectionsBuilder, milisecsToWait, { options: { skip: newSkip, limit: 100 } });
  } else {
    connectionsBuilder({ options: { skip: newSkip, limit: 100 } });
  }
};

async function getTargetUser() {
  targetUser = await findOneFriend({ screen_name: 'sherlaimov' });
  console.log(targetUser);
  connectionsBuilder({});
}

// getTargetUser();

// connectionsBuilder({});
