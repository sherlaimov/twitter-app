export const profileTargetWords = [
  'developer',
  'engineer',
  'open source',
  'programming',
  'programmer',
  'software engineer',
  'C#',
  'C++',
  'js',
  'Scala',
  'Erlang',
  'Haskell',
  'hacker',
  'JavaScript',
  'java',
  'devtools',
  'Frontend',
  'Chrome',
  'NodeJs',
  'script',
  'code',
  'coding',
  '.net',
  'reactjs',
  'angular',
  'DevOps',
  'Linux',
  'sysadmin',
  'WebDev',
  'css',
  'html',
  'machine learning',
  'data scientist',
  'react-sketchapp',
  'augmented reality',
  'react app',
  'webpack',
  'npm',
  'computer science',
  'vue',
  'vuejs',
  'react',
  'cto',
  'scientist',
  'hacking',
  'github',
  'algorithm'
];

export const tweetTargetWords = [
  'program',
  'programme',
  'software',
  'AR',
  'iOS',
  'Android',
  'privacy',
  'tech',
  'computer',
  'multithreading',
  'design',
  'versioning',
  'CI',
  'MVP',
  'refactoring',
  'app',
  'web',
  'reverse-engineer',
  'website',
  'pull request',
  'computing',
  'pageload',
  'runtime',
];
