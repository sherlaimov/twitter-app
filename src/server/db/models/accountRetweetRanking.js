import mongoose from '../mongo';

const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const accountPopularitySchema = new Schema(
  {
    screen_name: { type: String, required: true, unique: true },
    tweetToRetweetRatio: Number,
    avgRetweetCount: Number,
    account: { type: Schema.Types.ObjectId, ref: 'filtered_accounts' }
  },
  { timestamps: true }
);
const accountRetweetRanking = mongoose.model('account_retweet_ranking', accountPopularitySchema);
// filteredTweets.watch().on('change', change => console.log(change));

export default accountRetweetRanking;
