import mongoose from '../mongo';

const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const friendSchema = new mongoose.Schema(
  {
    user_twitter_id: { type: String, required: true },
    screen_name: { type: String, required: true },
    description: String,
    followers_count: Number,
    friends_count: Number,
    location: String,
    verified: Boolean,
    statuses_count: Number,
    ranking: Number,
    from_friends_list: { type: Schema.Types.ObjectId, ref: 'connections' }
  },
  { timestamps: true }
);
friendSchema.index({ user_twitter_id: 1, from_friends_list: 1 }, { unique: true });
const friend = mongoose.model('friend', friendSchema);

export default friend;
