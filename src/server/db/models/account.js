import mongoose from '../mongo';

const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const acountsSchema = new mongoose.Schema(
  {
    user_twitter_id: { type: String, required: true, unique: true },
    screen_name: { type: String, required: true, unique: true },
    description: String,
    followers_count: Number,
    friends_count: Number,
    location: String,
    verified: Boolean,
    statuses_count: Number,
    ranking: Number,
    account_retweet_ranking: { type: Schema.Types.ObjectId, ref: 'account_retweet_ranking' },
    parsed_timeline: { type: Boolean, default: false },
    parsed_friendslist: { type: Boolean, default: false }
  },
  { timestamps: true }
);
const filteredAccounts = mongoose.model('filtered_accounts', acountsSchema);
// filteredTweets.watch().on('change', change => console.log(change));

export default filteredAccounts;
