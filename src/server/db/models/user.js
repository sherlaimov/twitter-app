import mongoose from '../mongo';

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  },
  screen_name: { type: String, unique: true },
  twitterProvider: {
    id: String,
    token: String,
    tokenSecret: String

    // select: false
  }
});

UserSchema.set('toJSON', { getters: true, virtuals: true });

UserSchema.statics.upsertTwitterUser = function(token, tokenSecret, profile, cb) {
  const that = this;
  return this.findOne(
    {
      'twitterProvider.id': profile.id
    },
    (err, user) => {
      // no user was found, lets create a new one
      if (!user) {
        const newUser = new that({
          email: profile.emails[0].value,
          screen_name: profile.username,
          twitterProvider: {
            id: profile.id,
            token,
            tokenSecret
          }
        });

        newUser.save((error, savedUser) => {
          if (error) {
            console.log(error);
          }
          return cb(error, savedUser);
        });
      } else {
        return cb(err, user);
      }
    }
  );
};

const userModel = mongoose.model('User', UserSchema);

export default userModel;
