import mongoose from '../mongo';

mongoose.Promise = global.Promise;

const tweetSchema = new mongoose.Schema(
  {
    tweet_id: { type: String, required: true, unique: true },
    screen_name: String,
    full_text: { type: String },
    retweet_count: { type: Number },
    tweet_time: { type: String },
    retweeted: { type: Boolean },
    used: { type: Boolean, default: false },
    retweeted_by_app: { type: Boolean, default: false },
    file: [String]
  },
  { timestamps: true }
);
const filteredTweets = mongoose.model('filtered_tweets', tweetSchema);
// filteredTweets.watch().on('change', change => console.log(change));

export default filteredTweets;
