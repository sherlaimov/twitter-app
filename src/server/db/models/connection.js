import mongoose from '../mongo';

mongoose.Promise = global.Promise;

const connectionsSchema = new mongoose.Schema(
  {
    screen_name: { type: String, required: true, unique: true },
    connections: [
      {
        screen_name: { type: String, unique: true },
        user_twitter_id: { type: String, unique: true },
        relations: [String]
      }
    ]
  },
  { timestamps: true }
);
const connection = mongoose.model('connections', connectionsSchema);

export default connection;
