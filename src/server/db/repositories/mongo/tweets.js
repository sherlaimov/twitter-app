import Tweet from '../../models/tweet';

export function insertManyTweets(tweets, options) {
  return Tweet.insertMany(tweets, options);
}

export function findTweets({ condition, projection, options }) {
  return Tweet.find(condition, projection, options);
}

export function findTweetsByCondition({ condition, projection, options }) {
  return Tweet.find(condition, projection, options);
}
export function findOneAndUpdate({ condition, update, options }) {
  return Tweet.findOneAndUpdate(condition, update, options);
}
