import Friend from '../../models/friend';

export function insertManyFriends({ friends, options }) {
  return Friend.insertMany(friends, options);
}

export function findOneFriend(criteria) {
  return Friend.findOne(criteria);
}

export function createFriend(account) {
  return Friend.create(account);
}

export function findFriends({ condition, projection, options }) {
  return Friend.find(condition, projection, options);
}

export function updateFriend({ criteria, value }) {
  return Friend.update(criteria, value);
}
