import AccountRetweetRanking from '../../models/accountRetweetRanking';

export function createAccountRanking(rankings) {
  return AccountRetweetRanking.create(rankings);
}

export function findOneRetweetRanking(criteria) {
  return AccountRetweetRanking.findOne(criteria);
}
