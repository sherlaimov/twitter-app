import Connection from '../../models/connection';

export function findOneConnectionAndUpdate({ condition, update, options }) {
  return Connection.findOneAndUpdate(condition, update, options);
}

export function findConnections({ condition, projection, options }) {
  return Connection.find(condition, projection, options);
}

export function findOneConnection({ condition, projection, options }) {
  return Connection.findOne(condition, projection, options);
}
