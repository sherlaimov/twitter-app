import Account from '../../models/account';

export function insertManyAccounts(accounts, options) {
  return Account.insertMany(accounts, options);
}

export function findOneAccount(criteria) {
  return Account.findOne(criteria);
}

export function createAccount(account) {
  return Account.create(account);
}

export function findAccountsByCondition({ condition, projection, options }) {
  return Account.find(condition, projection, options);
}

export function updateAccount({ criteria, value }) {
  return Account.update(criteria, value);
}
