import db from '../../sql';

const table = () => db('filtered_tweets');

export default function recordTweets(tweets) {
  return table()
    .insert(tweets)
    .then(([id]) => {
      console.log(id);
      return id;
    });
}
