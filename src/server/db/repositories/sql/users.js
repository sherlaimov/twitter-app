import db from '../../sql';

const table = () => db('filtered_users');

export default function insertNewUsers(users) {
  return table()
    .insert(users)
    .then(([id]) => {
      console.log('=======> INSIDE INSERT');
      console.log(id);
      return id;
    });
}
// [ 65 ]
// { method: 'insert',
//   options: {},
//   timeout: false,
//   cancelOnTimeout: false,
//   bindings:
//    [ 'Cofounder @withspectrum 💬Advisor @EducativeInc 👨🏼‍🏫 Makes styled-components, react-boilerplate and micro-analytics 💅  Coffee geek, skier, traveller ☕️ he/him',
//      19014,
//      900,
//      'Vienna, Austria',
//      'mxstbr',
//      29954,
//      '2451223458',
//      false,
//      'JavaScript ninja, image processing expert, software quality fanatic. VP of Eng @cypress_io MS MVP for OSS',
//      4586,
//      1040,
//      'Boston',
//      'bahmutov',
//      11183,
//      '30477646',
//      false,
//      'I\'m a software engineer working on the @reactjs core team at @facebook. I\'m the author of @inferno_js and other libraries. I enjoy coding + being a Dad.',
//      10332,
//      413,
//      'London',
//      'trueadm',
//      2511,
//      '39141147',
//      false,
//      '@NodeJS collaborator • @WorkerConf organizer • @v8js engineer • Probably broke the web for you once or twice • Husband & Father • Opinions my own',
//      7742,
//      1207,
//      'Munich',
//      'bmeurer',
//      6790,
//      '306879348',
//      false,
//      'Web frontend expert at @futurice. Mostly I tweet about Elm, ES6, and React Native. I use Elm at work, and organize @elmsinki. http://ohanhi.com',
//      2772,
//      149,
//      'Helsinki, Finland',
//      'ohanhi',
//      1431,
//      '1367521166',
//      false,
//      '▼・ᴥ・▼ JavaScript & Elixir sommelier, 🚀 blazing fast eng. manager @netflix, editor of https://emberway.io, and alpaca expert | https://github.com/poteto',
//      7098,
//      592,
//      'SF Bae Area',
//      'sugarpirate_',
//      1686,
//      '2832427459',
//      false,
//      'Front-End Core engineering lead @IndeedEng. Tweets are my own.',
//      24000,
//      548,
//      'Austin, TX',
//      'rmurphey',
//      18165,
//      '6490602',
//      false ],
