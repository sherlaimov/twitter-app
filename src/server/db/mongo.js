import mongoose from 'mongoose';
import config from '../config';

mongoose.connect(config.db.mongo.url);
// {
//   useNewUrlParser: true,
//   port: 27017
// }
//  https://stackoverflow.com/questions/51156334/unhandled-rejection-mongoerror-port-must-be-specified/51159273
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('mongoDB connected');
});

export default mongoose;
