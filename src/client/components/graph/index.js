import React, { Component } from 'react';
import * as d3 from 'd3';
import { Graph } from 'react-d3-graph';
import { Node } from 'react-d3-graph';
import './style.css';
// graph payload (with minimalist structure)
const testData = {
  nodes: [{ id: 'Harry', size: 500 }, { id: 'Sally' }, { id: 'Alice' }],
  links: [
    { source: 'Harry', target: 'Sally' },
    { source: 'Sally', target: 'Harry' },
    { source: 'Harry', target: 'Alice' }
  ]
};

// the graph configuration, you only need to pass down properties
// that you want to override, otherwise default ones will be used
const myConfig = {
  nodeHighlightBehavior: true,
  node: {
    color: 'lightgreen',
    size: 120,
    highlightStrokeColor: 'blue'
  },
  link: {
    highlightColor: 'lightblue'
  }
};

export default class TestGraph extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   nodes: null,
    //   links: null
    // };
  }

  mapData = data => {
    const { connections, screen_name } = data;
    const nodes = connections.map(({ screen_name: id }) => ({ id }));
    nodes.push({ id: screen_name });
    const links = nodes.map(({ id }) => ({ source: screen_name, target: id }));
    this.setState({ nodes, links });
  };

  componentDidMount() {
    const { screenName, token } = this.props;
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    fetch(`/twitter/graph/${screenName}`, {
      headers
    })
      .then(res => res.json())
      .then(data => this.mapData(data));
  }

  render() {
    const nodes = this.state;
    console.log({ nodes });
    const isReady = nodes !== undefined && nodes !== null;
    return (
      <div>
        {isReady && (
          <Graph
            id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
            data={nodes}
            config={myConfig}
            onClickNode={onClickNode}
            onClickLink={onClickLink}
          />
        )}
      </div>
    );
  }
}
// onMouseOverNode={onMouseOverNode}
// onMouseOutNode={onMouseOutNode}
// onMouseOverLink={onMouseOverLink}
// onMouseOutLink={onMouseOutLink}
// graph event callbacks
const onClickNode = function(nodeId) {
  window.alert('Clicked node ${nodeId}');
};

const onMouseOverNode = function(nodeId) {
  window.alert(`Mouse over node ${nodeId}`);
};

const onMouseOutNode = function(nodeId) {
  window.alert(`Mouse out node ${nodeId}`);
};

const onClickLink = function(source, target) {
  window.alert(`Clicked link between ${source} and ${target}`);
};

const onMouseOverLink = function(source, target) {
  window.alert(`Mouse over in link between ${source} and ${target}`);
};

const onMouseOutLink = function(source, target) {
  window.alert(`Mouse out link between ${source} and ${target}`);
};
