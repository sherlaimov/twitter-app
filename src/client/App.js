import React, { Component } from 'react';
import Graph from './components/graph';
import TwitterLogin from 'react-twitter-auth';
// import TwitterLogin from './components/twitterLogin/index';

class App extends Component {
  constructor() {
    super();
    this.loginUrl = 'http://localhost:3000/auth/twitter-callback';
    this.requestTokenUrl = 'http://localhost:3000/auth/twitter/reverse';
    this.state = { isAuthenticated: false, user: null, token: '' };
  }

  onSuccess = response => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {
      if (token) {
        this.setState({ isAuthenticated: true, user: user, token: token });
      }
    });
  };

  onFailed = error => {
    console.log(error);
  };

  logout = () => {
    this.setState({ isAuthenticated: false, token: '', user: null });
  };

  render() {
    const { token, isAuthenticated, user } = this.state;
    const content = isAuthenticated ? (
      <div>
        <p>Authenticated</p>
        <div>{user.email}</div>
        <Graph screenName={user.screen_name} token={token} />
        <div>
          <button onClick={this.logout} className="button">
            Log out
          </button>
        </div>
      </div>
    ) : (
      <TwitterLogin
        loginUrl={this.loginUrl}
        onFailure={this.onFailed}
        onSuccess={this.onSuccess}
        requestTokenUrl={this.requestTokenUrl}
      />
    );

    return <div className="App">{content}</div>;
  }
}

export default App;
