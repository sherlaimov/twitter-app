## Twitter Graph App

`npm run dev` - для запуска

Для получения графа необходимо пройти авторизацию через Твиттер.

Сейчас есть 2 используемых роута:

1. `twitterAuth` - для авторизации и получения пользовательских *oauth_token* и _oauth_token_secret_, которые записываются в `process.env.TWI_ACCESS_TOKEN_KEY` и `process.env.TWI_ACCESS_TOKEN_SECRET` соответственно, чтобы использоваться далее при запросах к Twitter API. (подробнее - [User authentication aka user context])
2. `twitter` - для получения связей пользователя. Задействованы два сервиса - `FriendshipParser` и `ConnectionsBuilder`. Оба получают на вход mongo-пользователя. 

`FriendshipParser` - получает друзей пользователя от Twitter API и записывает в базу. Сейчас для удобства калибровки останавливается на 40 пользователей (2 запроса) и эммитет событие `received-friends`. На будущее подумать - как правильно организовать обхождение ограничений Twitter API: сначала отдаем часть пользователей, далее сигнализируем об обновлении графа. Twitter эндпоинт (`/friends/list.json`) позволяет сделать 15 обращений к API в течение 15 минут. 

`ConnectionsBuilder` - пингует базу на наличие новых друзей и, если находит, посылает запрос на `/friendships/lookup.json` для получения информации об отношениях пользователей. Останавливается по событию `received-friends`. Эмиттит событие `connections-built` после которого роут ответит пользователю с данными для графа.
  

   [User authentication aka user context]: <https://developer.twitter.com/en/docs/basics/authentication/overview/oauth>